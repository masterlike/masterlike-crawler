var PostCreator = require('./services/PostCreator');
var PageCrawler = require('./services/PageCrawler');
var PageLister = require('./services/PageLister');

var PageMainCrawler = function () {
  return {
    start: function () {
      console.log('Page Crawler started!');
      var pageLister = new PageLister();
      pageLister.list(this.onPagesListed.bind(this));
    },
    onPagesListed: function (err, pages) {
      pages.forEach(function (page) {
        console.log('Crawling for new posts on ' + page.name + ' page...');
        this.listPosts(page.access_token, page.page_id);
      }.bind(this));
    },
    listPosts: function (pageToken, pageId) {
      var pageCrawler = new PageCrawler(pageToken);
      pageCrawler.listPosts(pageId, this.onPostsListed.bind(this));
    },
    onPostsListed: function (pageId, posts) {
      posts.forEach(function (post) {
        this.createPost(pageId, post);
      }.bind(this));
    },
    createPost: function (pageId, params, callback) {
      var postCreator = new PostCreator();
      postCreator.create(pageId, params, this.onPostCreated.bind(this));
    },
    onPostCreated: function (err) {
      if (err) { return; }
      console.log('Post Created');
    },
  };
};

module.exports = PageMainCrawler;
