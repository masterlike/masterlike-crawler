var request = require('../utils/request');

var PageCrawler = function (token) {
  return {
		token: token,
		limit: 50,
    listPosts: function (pageId, callback) {
      var path = '/' + pageId;
      path += '?fields=posts.limit(' + this.limit + ')';
      path += '&access_token=' + this.token;
      var options = {
        method: 'GET',
    		host: 'graph.facebook.com',
    		path: path,
    	};
    	request(options, function (res) {
				this.onListedPosts(pageId, res, callback);
			}.bind(this));
    },
		onListedPosts: function(pageId, res, callback) {
			if(res.error) {
				console.error('PageCrawler - ListPosts - Error', res);
				callback(pageId, []);
				return;
			}
			callback(pageId, res.posts.data);
		},
  };
};

module.exports = PageCrawler;
