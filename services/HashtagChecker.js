var Page = require('../models/Page');

var HashtagChecker = function (hashtag) {
  return {
    hashtag: hashtag || '#masterlike',
    check: function (message, callback) {
      var included = message.includes(this.hashtag);
      callback(included);
    }
  };
};

module.exports = HashtagChecker;
