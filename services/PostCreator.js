var Post = require('../models/Post');

var PostCreator = function () {
  return {
    create: function (pageId, params, callback) {
      var post = new Post({
        post_id: params.id,
        page_id: pageId,
        message: params.message,
        story: params.story,
        created_time: new Date(params.created_time),
      });
      post.save(callback);
    },
  };
};

module.exports = PostCreator;
