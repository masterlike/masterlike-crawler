var Post = require('../models/Post');

var PostLister = function () {
  return {
    list: function (pageId, pageToken, callback) {
      Post.find({ page_id: pageId }, function(err, res) {
      	callback(pageId, pageToken, err, res);
      }.bind(this));
    }
  };
};

module.exports = PostLister;
