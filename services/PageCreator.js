var Page = require('../models/Page');

var PageCreator = function () {
  return {
    create: function (pageId, params, callback) {
      var page = new Page({
        page_id: params.id,
        name: params.name,
        about: params.about,
        access_token: params.access_token,
        category: params.category,
        description_html: params.description_html,
        username: params.username,
        website: params.website,
      });
      page.save(callback);
    },
  };
};

module.exports = PageCreator;
