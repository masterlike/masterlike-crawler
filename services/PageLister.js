var Page = require('../models/Page');

var PageLister = function () {
  return {
    list: function (callback) {
      Page.find({}, callback);
    }
  };
};

module.exports = PageLister;
