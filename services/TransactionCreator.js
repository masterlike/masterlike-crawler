var Transaction = require('../models/Transaction');

var TransactionCreator = function () {
  return {
    create: function (comment_id, post_id, user_id,  page_id, link, payed, notified, payment, callback) {
      var transaction = new Transaction({
      	comment_id: comment_id,
      	post_id: post_id,
      	user_id: user_id,
      	page_id: page_id,
      	link: link,
      	payed: payed,
      	notified: notified,
        confirmed: false,
      	payment: payment
      });

      transaction.save(callback);
    },
  };
};

module.exports = TransactionCreator;
