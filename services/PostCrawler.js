var request = require('../utils/request');

var PostCrawler = function (token) {
  return {
    token: token,
    limit: 50,
    listComments: function (postId, callback, pageId) {
      var path = '/' + postId + '/comments?&access_token=' + this.token;
      var options = {
        method: 'GET',
        host: 'graph.facebook.com',
        path: path,
      };
      request(options, function (res) {
        this.onListedComments(postId, res, callback, pageId);
      }.bind(this));
    },
    onListedComments: function(postId, res, callback, pageId) {
      if(res.error) {
        console.error('PageCrawler - ListPosts - Error', res);
        callback(pageId, []);
        return;
      }

      callback(postId, res, pageId);
    },
  };
};

module.exports = PostCrawler;
