var https = require('https');

var request = function (options, callback) {
	var req = https.request(options, function (res) {
		var body = '';

		res.setEncoding('utf8');

		res.on('data', function (chunk) {
			body += chunk;
		});

		res.on('end', function () {
			if (callback) {
				callback(JSON.parse(body));
			}
		});
	});

	req.on('error', function (err) {
		if (callback) {
			callback(e);
		}
	});

	req.write('data\n');
	req.end();
};

module.exports = request;
