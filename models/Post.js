var timestamps = require('mongoose-timestamp');
var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
  post_id: { type : String, unique: true, required: true, dropDups: true, },
  page_id: { type : String, required: true, },
  message: String,
  created_time: Date,
  story: String,
});

PostSchema.plugin(timestamps);

module.exports = mongoose.model('Post', PostSchema);
