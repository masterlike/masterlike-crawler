var timestamps = require('mongoose-timestamp');
var mongoose = require('mongoose');

var PageSchema = new mongoose.Schema({
  page_id: { type : String, unique: true, required: true, dropDups: true, },
  name: { type : String, required: true, },
  about: { type : String, },
  access_token: { type : String, required: true, },
  category: { type : String, },
  description_html: { type : String, },
  username: { type : String, required: true, },
  website: { type : String, },
});

PageSchema.plugin(timestamps);

module.exports = mongoose.model('Page', PageSchema);
