var timestamps = require('mongoose-timestamp');
var mongoose = require('mongoose');

var TransactionSchema = new mongoose.Schema({
    comment_id: { type : String, unique: true, required: true, dropDups: true, },
    post_id: String,
    user_id: String,
    page_id: String,
    link: String,
    payed: Boolean,
    notified: Boolean,
    confirmed: Boolean,
    payment: Number,
    buyer_id: String,
});

TransactionSchema.plugin(timestamps);

module.exports = mongoose.model('Transaction', TransactionSchema);
