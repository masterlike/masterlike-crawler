var TransactionCreator = require('./services/TransactionCreator');
var PageLister = require('./services/PageLister');
var PostLister = require('./services/PostLister');
var PostCrawler = require('./services/PostCrawler');
var HashtagChecker = require('./services/HashtagChecker');
var shortid = require('shortid');

var PostMainCrawler = function () {
  return {
    start: function () {
      console.log('Post Crawler started!');
      var pageLister = new PageLister();
      pageLister.list(this.onPagesListed.bind(this));
    },
    onPagesListed: function (err, pages) {
      pages.forEach(function (page) {
        var postLister = new PostLister();
        postLister.list(page.page_id, page.access_token, this.onPostsListed.bind(this));
      }.bind(this));
    },
    onPostsListed: function (pageId, pageToken, err, posts) {
      posts.forEach(function (post) {
        var postCrawler = new PostCrawler(pageToken);
        postCrawler.listComments(post.post_id, this.onCommentsListed.bind(this), pageId);
      }.bind(this));
    },
    onCommentsListed: function (postId, comments, pageId) {
      comments.data.forEach(function (comment) {
        var hashtagChecker = new HashtagChecker();
        var createTransaction = this.createTransaction;

        hashtagChecker.check(comment.message, function (res) {
          console.log('Comment detected ' + comment.message + '...');
          if (res) {
            createTransaction(comment, postId, pageId);
          }
        });
      }.bind(this));
    },
    createTransaction: function (comment, postId, pageId) {
      var transactionCreator = new TransactionCreator();
      transactionCreator.create(comment.id, postId, comment.from.id,  pageId, 'http://localhost:3000/t/' + shortid.generate(), false, false, 5);
    },
    onTransactionCreated: function (err) {
      if (err) {
        return;
      }
      console.log('Transaction Created');
    }
  };
};

module.exports = PostMainCrawler;
