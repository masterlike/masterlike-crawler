var mongoose = require('mongoose');
var assert = require('assert');
var fs = require('fs');

var App = require('./App');

var MONGODB_URL = "mongodb://master:like123@aws-us-east-1-portal.19.dblayer.com:10348,aws-us-east-1-portal.20.dblayer.com:10348/masterlike";

var MONGO_OPTIONS = {
  mongos: {}
};

mongoose.connection.on('error',function (err) {
  console.log('Mongoose default connection error: ' + err);
});

mongoose.connection.on('open', function (err) {
  console.log('Started!');
  app = new App();
  app.start();
});

mongoose.connect(MONGODB_URL, MONGO_OPTIONS);
