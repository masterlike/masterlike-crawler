var PageMainCrawler = require('./PageMainCrawler');
var PostMainCrawler = require('./PostMainCrawler');

var App = function () {
  return {
    start: function () {
      // Starts - Page Crawler
      setInterval(function() {
        var pageCrawler = new PageMainCrawler();
        pageCrawler.start();
      }, 10 * 1000);

      // Starts - Post Crawler
      setInterval(function() {
        var postCrawler = new PostMainCrawler();
        postCrawler.start();
      }, 10 * 1000);
    },
  };
};

module.exports = App;
